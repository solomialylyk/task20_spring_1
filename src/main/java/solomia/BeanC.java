package solomia;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanC implements BeanValidator{
    private static Logger log = LogManager.getLogger(BeanB.class);
    private String name;
    private int value;

    public BeanC(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public void validate() {
        System.out.println("Validator BeanA");
        if (getValue() >= 0) {
            log.info("Validate successful : A");
        } else {
            log.info("Validate error");
        }
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
