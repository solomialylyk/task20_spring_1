package solomia;

public class BeanE {
    private String name;
    private int value;

    public BeanE() {
    }

    public BeanE(BeanA beanBC) {
        this.name = beanBC.getName();
        this.value = beanBC.getValue();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
