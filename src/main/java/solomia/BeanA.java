package solomia;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanA implements BeanValidator{
    private static Logger log = LogManager.getLogger(BeanA.class);
    private String name;
    private int value;

    public BeanA() {
    }

    public BeanA(BeanB beanB, BeanC beanC) {
        this.name = beanB.getName();
        this.value = beanC.getValue();
    }

    public BeanA(BeanB beanB, BeanD beanD) {
        this.name = beanB.getName();
        this.value = beanD.getValue();
    }

    public BeanA(BeanC beanC, BeanD beanD) {
        this.name = beanC.getName();
        this.value = beanD.getValue();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public void validate() {
        System.out.println("Validator BeanA");
        if (getValue() >= 0) {
            log.info("Validate successful : A");
        } else {
            log.error("Validate error");
        }
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
