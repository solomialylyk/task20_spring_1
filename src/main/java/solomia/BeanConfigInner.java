package solomia;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfigInner {
    @Bean
    public  BeanE getMyBeanE() {
        BeanE bean = new BeanE();
        bean.setName("Cat");
        bean.setValue(5);
        return bean;
    }
}
