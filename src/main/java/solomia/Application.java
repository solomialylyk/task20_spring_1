package solomia;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfig.class);
//        BeanA bean = context.getBean(BeanA.class);
//        System.out.println(bean);
//        System.out.println(System.identityHashCode(bean));

        BeanB bean2 = context.getBean(BeanB.class);
        System.out.println(bean2);
        System.out.println(System.identityHashCode(bean2));

        BeanC bean3 = context.getBean(BeanC.class);
        System.out.println(bean3);
        System.out.println(System.identityHashCode(bean3));

        BeanD bean4 = context.getBean(BeanD.class);
        System.out.println(bean4);
        System.out.println(System.identityHashCode(bean4));

//        BeanE bean5 = context.getBean(BeanE.class);
//        System.out.println(bean5);
//        System.out.println(System.identityHashCode(bean5));

        BeanF bean6 = context.getBean(BeanF.class);
        System.out.println(bean6);
        System.out.println(System.identityHashCode(bean6));

        BeanA bean7 = context.getBean("getBeanBC", BeanA.class);
        System.out.println(bean7);
        System.out.println(System.identityHashCode(bean7));

        BeanE bean8 = context.getBean("getBeanE", BeanE.class);
        System.out.println(bean8);
        System.out.println(System.identityHashCode(bean8));


    }
}
