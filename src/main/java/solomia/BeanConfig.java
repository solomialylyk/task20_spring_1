package solomia;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

@Configuration
@Import(BeanConfigInner.class)
@PropertySource("myD.properties")
@PropertySource("myB.properties")
@PropertySource("myC.properties")
public class BeanConfig {

    @Bean
    @DependsOn(value = {"beanD", "beanB", "beanC"})
    public BeanA getBeanBC(BeanB getBeanB, BeanC getBeanC) {
        return new BeanA(getBeanB, getBeanC);
    }

    @Bean
    public BeanA getBeanBD(@Qualifier("beanB") BeanB beanB, @Qualifier("beanD") BeanD beanD) {
        return new BeanA(beanB, beanD);
    }

    @Bean
    public BeanA getBeanCD(@Qualifier("beanC") BeanC beanC, @Qualifier("beanD") BeanD beanD) {
        return new BeanA(beanC, beanD);
    }

    @Bean
    public BeanE getBeanE(BeanA getBeanBC) {
        return new BeanE(getBeanBC);
    }

    @Bean
    public BeanE getBeanE2(BeanA getBeanBD) {
        return new BeanE(getBeanBD);
    }

    @Bean
    public BeanE getBeanE3(@Qualifier("getBeanCD") BeanA bean) {
        return new BeanE(bean);
    }

    @Bean("beanA")
    public BeanA getMyBeanA() {
        BeanA bean = new BeanA();
        bean.setName("Pig");
        bean.setValue(1);
        return bean;
    }

    @Bean("beanF")
    public BeanF getMyBeanF() {
        BeanF bean = new BeanF();
        bean.setName("Parrot");
        bean.setValue(6);
        return bean;
    }

    @Value("${beanB.name}")
    private String nameB;
    @Value("${beanB.value}")
    private int valueB;
    @Bean("beanB")
    public BeanB getMyBeanB() {
        return new BeanB(nameB, valueB);
    }

    @Value("${beanC.name}")
    private String nameC;
    @Value("${beanC.value}")
    private int valueC;
    @Bean("beanC")
    public BeanC getMyBeanC() {
        return new BeanC(nameC, valueC);
    }

    @Value("${beanD.name}")
    private String nameD;
    @Value("${beanD.value}")
    private int valueD;
    @Bean("beanD")
    public BeanD getMyBeanD() {
        return new BeanD(nameD, valueD);
    }
}
